module Jekyll
  module UtilityMethods
    def dash_camelcase(input)
      input.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
    end
  end
end

Liquid::Template.register_filter(Jekyll::UtilityMethods)
